use serde_json::Value;
use std::fs::File;
use std::io::BufReader;
use super::BoxResult;
use std::io::prelude::*;

/**
 * Stores the composer.json content in a generic untyped Value.
 */
pub struct Composer {
    content: Value
}

impl Composer {

    /**
     * Read composer.json and create a composer struct instance
     */
    pub fn read_file(filename: &str) -> BoxResult<Composer> {
        let file = File::open(filename)?;
        let reader = BufReader::new(file);
        Ok(Composer{ content: serde_json::from_reader(reader)? })
    }

    /**
     * Current composer is modified according to output string.
     * All lines should contain
     */
    pub fn update_require(&mut self, output: &str) -> BoxResult<i32> {
        
        let mut lines = 0;
        let plugins: Value = serde_json::from_str(output)?;
        for p in plugins.as_array().unwrap() {
            let package: &str = p["update_id"].as_str().unwrap();
            self.content["require"][package] = p["version"].clone();
            lines += 1;
        }
        Ok(lines)
    }

    pub fn write_file(self, filename: &str) -> BoxResult<String> {
        let mut file = File::create(filename)?;
        let json = serde_json::to_string_pretty(&self.content)?;
        file.write_all(&json.as_bytes())?;
        Ok("Write OK".to_string())
    }
}



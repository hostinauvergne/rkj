use std::process:: { Command, Output };
use super::BoxResult;

pub struct Ssh {
    host: String,
    dir: String
}

impl Ssh {
    pub fn from_url(url: &str) -> Ssh {
        debug!("Create a ssh client to connect to {}", &url);
        match url.find(':') {
            Some(position) => {
                Ssh {
                    host: String::from(&url[0..position]),
                    dir: String::from(&url[(position+1)..])
                }
            },
            None => {
                info!("Invalid ssh url missing :");
                Ssh {
                    host: String::from(&url[..]),
                    dir: "~".to_string()
                }
            }
        }
    }

    pub fn exec(&self, cmd: &str) -> BoxResult<String> {

        debug!("ssh {}; cd {}; {}", &self.host, &self.dir, cmd);

        let output = Command::new("ssh")
            .args(&["-tt", &self.host, &format!("cd {};{}", &self.dir, cmd)])
            .output()?;
            
        Ok(self.result(output)?)
    }

    pub fn copy(&self, from: &str, to: &str) -> BoxResult<String> {
        let dir = if from.chars().next().unwrap_or('/') == '/' {
            format!("{}", from)
        } else {
            format!("{}/{}", self.dir, from)
        };

        debug!("scp {}:{} {}", &self.host, dir, to);

        let output = Command::new("scp")
            .args(&["-rp", &format!("{}:{}", &self.host, dir), to])
            .output()?;

        Ok(self.result(output)?)
    }

    pub fn rsync(&self, from: &str, to: &str) -> BoxResult<String> {
        let dir = if from.chars().next().unwrap_or('/') == '/' {
            format!("{}", from)
        } else {
            format!("{}/{}", self.dir, from)
        };

        debug!("rsync -avz {}:{} {}", &self.host, dir, to);

        let output = Command::new("rsync")
            .args(&["-avz", &format!("{}:{}", &self.host, dir), to])
            .output()?;

        Ok(self.result(output)?)
    }


    fn result(&self, output :Output) -> BoxResult<String> {
        if !output.status.success() {
            warn!("stdout : {} ", String::from_utf8(output.stdout).expect("Invalid stdout"));
            warn!("stderr : {} ", String::from_utf8(output.stderr).expect("Invalid stderr"));
            bail!("Command failed");
        }

        Ok(String::from_utf8(output.stdout).expect("Invalid stdout"))
    }    
}

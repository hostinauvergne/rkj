#[macro_use]
extern crate clap;
#[macro_use]
extern crate log;
#[macro_use]
extern crate simple_error;
extern crate simple_logger;
extern crate chrono;
extern crate serde_json;

use clap::{App, Arg, SubCommand};
use log::Level;
use std::error::Error;
mod wordpress;
pub mod ssh;
pub mod composer;

/**
 * Enum for main command.
 */ 
arg_enum! {
    #[derive(Debug)]
    enum Commands {
        Init,
        Run,
        Wordpress
    }
}

// type for generic errors
pub type BoxResult<T> = Result<T,Box<Error>>;

fn main() {
    let matches = App::new(option_env!("CARGO_PKG_NAME").unwrap_or("rkj"))
        .version(option_env!("CARGO_PKG_VERSION").unwrap_or("unknwon"))
        .author(option_env!("CARGO_PKG_AUTHORS").unwrap_or("unknwon"))
        .about(option_env!("CARGO_PKG_DESCRIPTION").unwrap_or("unknwon"))
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .subcommand(SubCommand::with_name("wordpress")
            .about("Wordpress commands")
            .subcommand(SubCommand::with_name("ssh-copy")
                .about("Copy a wordpress from ssh")
                .arg(
                    Arg::with_name("ssh")
                        .help("SSH path as user@host:/path")
                        .required(true)
                )
            )
        )
        .get_matches();

    let verbose = matches.occurrences_of("v");
    let level = match verbose {
        0 => Level::Error,
        1 => Level::Info, // And Warn
        2 | _ => Level::Debug // And Trace
    };
    simple_logger::init_with_level(level).unwrap();

    // Main 
    match handle(&matches) {
        Ok(message) => { 
            println!("{}", message); 
        },
        Err(error) => {
            error!("RKJ ends with an error {:?}", error);
            std::process::exit(1);
        }
    }
}

/**
 * Handle main command
 */
pub fn handle(matches: &clap::ArgMatches) -> BoxResult<String> {
    match matches.subcommand_name() {
        Some("wordpress")   => {
            let result = wordpress::handle(matches.subcommand_matches("wordpress").unwrap())?;
            Ok(result)
        },
        _ => bail!("Unkown command")
    } 
}
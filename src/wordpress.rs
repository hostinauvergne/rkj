use super::ssh::Ssh;
use super::composer::Composer;
use super::BoxResult;
use chrono::{DateTime, Utc};

// List of const strings : to read from a config file.
const COMPOSER_JSON: &str = "app/composer.json";
const SQL: &str = "sql/initdb";
const WP_CONTENT: &str = "app/web/app";

/**
 * Main function to handle sub command of wordpress
 */
pub fn handle(matches: &clap::ArgMatches) -> BoxResult<String> {
    debug!("Wordpress command launched");
    match matches.subcommand_name() {
        Some("ssh-copy") => {
            let ssh = matches.subcommand_matches("ssh-copy").unwrap().value_of("ssh");
            let result = ssh_copy(ssh)?;
            Ok(result)
        },
        _ => bail!("Unkown sub-command")
    }
}

/**
 * Function to handle ssh-copy subcommand
 */
fn ssh_copy(ssh_option: Option<&str>) -> BoxResult<String> {
    let url = ssh_option.expect("Missing ssh attribute");
    debug!("Ready to ssh-copy a wordpress site at {}", &url);
    let ssh = Ssh::from_url(&url);

    debug!("{}", backup_sql(&ssh)?);
    debug!("{}", backup_uploads(&ssh)?);
    debug!("{}", backup_plugins(&ssh)?);

    Ok("ssh-copy done".to_string())
}

fn backup_sql(ssh: &Ssh) -> BoxResult<String> {
    let now: DateTime<Utc> = Utc::now();
    let file = format!("{}.sql", now.format("%Y-%m-%d-%H-%M-%S"));
    debug!("Copy database to {}/{}", SQL, file);

    ssh.exec(&format!("wp db export /tmp/{} --add-drop-table", file))?;
    ssh.copy(&format!("/tmp/{}", file), &format!("{}/{}", SQL, file))?;
    ssh.exec(&format!("rm -f /tmp/{}", file))?;

    Ok("OK : backup_sql".to_string())
}

fn backup_uploads(ssh: &Ssh) -> BoxResult<String> {
    debug!("Copy uploads to {}", WP_CONTENT);

    ssh.rsync("wp-content/uploads", WP_CONTENT)?;

    Ok("OK : backup_uploads".to_string())
}

fn backup_plugins(ssh: &Ssh) -> BoxResult<String> {
    debug!("Convert plugins to packages");

    let output = ssh.exec("wp plugin list --status=active --fields=update_id,version --format=json")?;

    let mut composer = Composer::read_file(COMPOSER_JSON)?;

    let lines = composer.update_require(&output)?;
    composer.write_file(COMPOSER_JSON)?;

    Ok(format!("{} plugins added to composer.json", lines))
}